About
-----
Vostok is a small set of customizations for the Android Open Source Project.
Unlike many other projects, it's just a set of patches on top of AOSP. Only
a few [own repositories](https://gitlab.com/groups/vostok) have been added.

Supported Devices
-----------------
| Name                         | Codename        | Branch            | Build  |
| ---------------------------- | --------------- | ----------------- | ------ |
| ASUS Nexus 7 2013 (Wi-Fi)    | flo             | android-7.1.1_r33 | N4F26X |
| HTC Nexus 9 (Wi-Fi)          | flounder        | android-7.1.1_r33 | N4F26X |
| LG Nexus 4                   | mako            | android-7.1.1_r33 | N4F26X |

Building
--------
Select branch and codename for your device from the table above.

Download the source code:

    repo init -u https://android.googlesource.com/platform/manifest -b branch
    mkdir -p .repo/local_manifests
    curl https://gitlab.com/vostok/vostok-7/raw/master/vostok.xml > .repo/local_manifests/vostok.xml
    repo sync

You'll need a lot of disk space and some patience.

Get proprietary binary drivers for your device.
For **flounder** you'll need to extract `vendor.img` from a
[factory image](https://developers.google.com/android/nexus/images)
(**flounder** is called **volantis** there) and flash it later (see below).
For other devices download and extract
[appropriate packages](https://developers.google.com/android/nexus/drivers).

Build:

    source build/envsetup.sh
    lunch aosp_codename-user
    ./vostok-7/apply # this applies all customizations
    make dist -j8

It will take some time to compile all the bits of the operating system. Output
images will be signed with test keys. Never flash those images! This is a huge
security risk because those keys are well-known.

You must generate your own set of release keys and keep them private:

    ./vostok-7/genkeys ../release-keys

Re-sign the images with your release keys:

    ./vostok-7/sign ../release-keys

You'll find the output in `out/dist/vostok-img.zip`. Time to run it!

Running
-------
Unlock the bootloader of your device, boot it into fastboot mode and flash the
images:

    fastboot flash vendor vendor.img       # for flounder only
    fastboot update vostok-img.zip

Welcome to your new system!
